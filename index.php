<?php
//FOR DEVELOPMENT ONLY
if ($_SERVER['SERVER_NAME'] == "burntcow.com")
{
	session_start(); 
	if ($_SESSION['user'] != "birthnest")
	{
		header("Location: http://www.burntcow.com/login.php"); 
		exit;
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Birthnest - Chicago Doula - Peyton Callahan</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="keywords" content="">
<meta name="description" content="">
<script src="code.js" type="text/javascript"></script>
<link href="styles.css" rel="styleSheet" type="text/css">
</head>
<body>
<center>
<br><br>
<table width="982" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td align="center">
		<br><br>
		<a href="#start"><img src="images/imgHeader.gif" height="323" width="975" border="0" alt="Birthnest - conceive - educate - nest - labor - birth - rejoice"></a>
		<br><br><br>
		<a href="#start"><img src="images/imgBirthQuote.gif" height="61" width="916" border="0" alt="Birth is not only about making babies. Birth is about making mothers ~ strong, competent, capable mothers who trust themselves and know their inner strength. - Barbara Katz Rothman"></a>
		<br><br><br><br>
		</td>
	</tr>
	<tr>
		<td><a name="start"></a>
		<div style="position: relative; left: 40px; width:110;"><img src="images/imgHdrDoula.gif" height="37" width="110" alt="doula"></div>
		<br><br>
		<div style="position: relative; left: 112px; width:810;" class="bodyText">Whether you are pregnant for the first time or fourth, interested or not in using medication, birthing in water or on land�you should have a doula!  Women become doulas because they believe every birthing woman should maximize the opportunity to have their desired birth experience.  Doulas not only support the mother, they support the entire family before, during and after birth.  During birth, a doula provides comfort measures, reassurance and advocacy. A doula does not offer medical advice or perform medical procedures.
		<p>Penny Simkin, a childbirth advocate, comprehensively states, "Physicians, midwives and nurses are responsible for monitoring labor, assessing the medical condition of the mother and baby, and treating complications when they arise.  But childbirth is also an emotional and spiritual experience with long-term impact on a woman's personal well being.  A doula is constantly aware that the mother and her partner will remember this experience throughout their lives.  By 'mothering the mother' during childbirth the doula supports the parents in having a positive and memorable birth experience."</p>
		<p>Research studies have shown significant benefits of a having a doula-attended childbirth.  Those benefits include: </p>
		<ul>
		<li>reduced labor time</li>
		<li>decreased instrumental deliveries</li>
		<li>decreased cesarean deliveries</li>
		<li>reduced use of epidurals and oxytocin</li>
		<li>decreased postpartum depression</li>
		<li>increased breastfeeding success</li>
		<li>increased birth satisfaction</li>
		</ul>
		</div>
		<br><br><br>
		<div style="position: relative; left: 40px; width:128;"><img src="images/imgHdrPeyton.gif" height="41" width="128" alt="peyton"></div>
		<br><br>
		<div style="position: relative; left: 112px; width:810;" class="bodyText">Peyton is an ALACE and DONA trained doula, currently obtaining an ALACE Childbirth Education certification.  She graduated from the University of Florida with Bachelor of Science in Psychology (and continues to be a huge Gator Fan).  Peyton is the founder and program director for twentyfoursevenmom&#153;, a social and educational club for new and expectant moms.  She and her husband are blessed with three children.  They have experienced a hospital birth with a cascade of interventions, a hospital birth without interventions or medication and a home waterbirth.
		<p>"It is a privilege to help women regain trust in their bodies.  By doing so, women are better prepared for welcoming and surrendering to the natural process of childbirth. I view the presence of a birth doula as a silent force. Someone knowing what you need before you ask for it.  Someone getting what you need when you do ask for it.  Someone constantly supporting you so you can focus and better connect with your partner.  Someone allowing your partner to participate at their comfort level.  Someone providing knowledgeable guidance about the birth process and explaining medical protocols and jargon.  Someone working with your medical staff to support your decisions. Someone creating an atmosphere of calm and confidence.</p>
		<p>Although I value natural childbirth, I completely and non-judgmentally support a mother's choice for pain management.  Your birth belongs to you.  Not me.  Not your doctor or midwife.  Not your relatives.  To you.  This is your birth."</p>
		</div>
		<br><br><br>
		<div style="z-index:999;position: relative; left: 40px; width:128;"><img src="images/imgHdrService.gif" height="28" width="128" alt="service"></div>
		<br><br>
		<div style="z-index:999;position: relative; left: 112px; width:810;" class="bodyText">Birth doulas either work privately, for a hospital or a birthing organization. Generally, services include a consultation, two prenatal visits, unlimited phone and email availability, presence at your birth (as early as you need until a couple of hours after you deliver) and a postpartum checkup.  Most doulas charge a fee of $400-1000 and many work on a sliding scale.  It is recommended that you interview several doulas to find the one that best connects with you and your partner. To inquire about Peyton's services and rates, please email her at <a href="mailto:peyton@birthnest.info">peyton@birthnest.info</a></div>
		<br><br><br>
		<div style="z-index:999;position: relative; left: 40px; width:130;"><img src="images/imgHdrContact.gif" height="27" width="130" alt="contact"></div>
		<br><br>
		<div style="z-index:999;position: relative; left: 112px; width:300;" class="bodyText">Peyton Callahan<br>
		773-230-2442<br>
		<a href="mailto:peyton@birthnest.info">peyton@birthnest.info</a>
		<p>More information about doulas and birth services can be found on these websites.<br>
		<a href="http://www.birthlink.com" target="_blank">www.birthlink.com</a><br>
		<a href="http://www.alace.org" target="_blank">www.alace.org</a><br>
		<a href="http://www.dona.org" target="_blank">www.dona.org</a><br>
		<a href="http://www.neidadoula.org" target="_blank">www.neidadoula.org</a><br>
		<a href="http://www.birthwaysinc.com" target="_blank">www.birthwaysinc.com</a></p>			
		</div>
		<div align="right">
		<div style="z-index:1;position: relative; top: -300px; width:616;"><img src="images/imgNest.jpg" height="371" width="616" border="0" alt="Birthnest - conceive - educate - nest - labor - birth - rejoice"></div>
		</div>
		<div style="z-index:1;position: relative; top: -230px; width:982;">
		<table width="982" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td width="120"><a href="javascript://" onMouseOver="rollIn('image1');" onMouseOut="rollOut('image1');"><img name="image1" src="images/img1.jpg" height="120" width="120" border="0" alt=""></td>
				<td>&nbsp;</td>
				<td width="120"><a href="javascript://" onMouseOver="rollIn('image2');" onMouseOut="rollOut('image2');"><img name="image2" src="images/img2.jpg" height="120" width="120" border="0" alt=""></td>
				<td>&nbsp;</td>
				<td width="120"><a href="javascript://" onMouseOver="rollIn('image3');" onMouseOut="rollOut('image3');"><img name="image3" src="images/img3.jpg" height="120" width="120" border="0" alt=""></td>
				<td>&nbsp;</td>
				<td width="120"><a href="javascript://" onMouseOver="rollIn('image4');" onMouseOut="rollOut('image4');"><img name="image4" src="images/img4.jpg" height="120" width="120" border="0" alt=""></td>
				<td>&nbsp;</td>
				<td width="120"><a href="javascript://" onMouseOver="rollIn('image5');" onMouseOut="rollOut('image5');"><img name="image5" src="images/img5.jpg" height="120" width="120" border="0" alt=""></td>
				<td>&nbsp;</td>
				<td width="120"><a href="javascript://" onMouseOver="rollIn('image6');" onMouseOut="rollOut('image6');"><img name="image6" src="images/img6.jpg" height="120" width="120" border="0" alt=""></td>
				<td>&nbsp;</td>
				<td width="120"><a href="javascript://" onMouseOver="rollIn('image7');" onMouseOut="rollOut('image7');"><img name="image7" src="images/img7.jpg" height="120" width="120" border="0" alt=""></td>
			</tr>
		</table>
		</div>
		</td>
	</tr>
</table>
</center>
</body>
</html>